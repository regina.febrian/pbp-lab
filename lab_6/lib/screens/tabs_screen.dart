import 'package:flutter/material.dart';
import './publikasi.dart';
import './donasi.dart';
import './updateCovid.dart';
import './kontak.dart';
import 'kritikSaran.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': KritikSaranScreen(),
        'title': 'Rumah Harapan🏠',
      },
      {
        'page': DonasiScreen(),
        'title': 'Donasi',
      },
      {
        'page': PublikasiScreen(),
        'title': 'Artikel',
      },
      {
        'page': UpdateCovidScreen(),
        'title': 'Update',
      },
      {
        'page': KontakScreen(),
        'title': 'Kontak',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
      ),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).accentColor,
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.money_rounded),
            title: Text('Donasi'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.article),
            title: Text('Artikel'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.coronavirus),
            title: Text('Update Covid'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.contact_phone),
            title: Text('Kontak Penting'),
          ),
        ],
      ),
    );
  }
}
