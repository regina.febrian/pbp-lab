import 'package:flutter/material.dart';

import '../widgets.dart';

class KritikSaranScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          'Kritik dan Saran',
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 35,
            color: Colors.blue,
          ),
        ),
        SizedBox(height: 8),
        Text(
          'Kami sangat menghargai keinginan Anda untuk membantu kami mengembangkan website yang lebih baik. Anda dapat memberikan kritik dan saran dengan menulis pesan melalui tombol di bawah ini.',
          style: TextStyle(fontSize: 18),
          textAlign: TextAlign.justify,
        ),
        SizedBox(height: 25),
        Text(
          'Kritik dan Saran dari Pengguna',
          style: TextStyle(
            fontSize: 24,
            color: Colors.blue,
          ),
          textAlign: TextAlign.center,
        ),
        CardWidget("Regina", "2021-11-05 23:50:47", "ini kritik dan saran"),
        CardWidget("siapaa", "2021-11-05 23:50:29", "kritik saran kedua"),
      ]),
    );
  }
}
