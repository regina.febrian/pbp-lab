import 'package:flutter/material.dart';
import './publikasi.dart';
import './donasi.dart';
import './update_covid.dart';
import './kontak.dart';
import './form_kritiksaran.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages = [];
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': KritikSaranForm(),
        'title': 'Form Kritik Saran',
      },
      {
        'page': DonasiScreen(),
        'title': 'Donasi',
      },
      {
        'page': PublikasiScreen(),
        'title': 'Publikasi',
      },
      {
        'page': UpdateCovidScreen(),
        'title': 'Update Covid',
      },
      {
        'page': KontakScreen(),
        'title': 'Kontak Penting',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title'] as String),
      ),
      body: _pages[_selectedPageIndex]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).shadowColor,
        currentIndex: _selectedPageIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.attach_money_sharp),
            title: Text('Donasi'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.article),
            title: Text('Publikasi'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.coronavirus),
            title: Text('Update Covid'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.contact_phone),
            title: Text('Kontak Penting'),
          ),
        ],
      ),
    );
  }
}
