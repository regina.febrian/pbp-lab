import 'package:flutter/material.dart';
import './screens/tabs_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rumah Harapan🏠 | Kritik dan Saran',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        bottomAppBarColor: Colors.lightBlue,
      ),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
    );
  }
}
