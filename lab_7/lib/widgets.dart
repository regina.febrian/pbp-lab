import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  CardWidget(this.nama, this.tanggal, this.pesan);
  final nama;
  final tanggal;
  final pesan;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 24.0,
        ),
        decoration: BoxDecoration(
          color: Colors.lightBlue,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text(
              nama,
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(tanggal),
            ),
            Text(
              pesan,
            ),
          ],
        ));
  }
}
